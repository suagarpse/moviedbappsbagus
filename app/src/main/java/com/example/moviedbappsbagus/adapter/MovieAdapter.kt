package com.example.moviedbappsbagus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviedbappsbagus.DetailMovieActivity
import com.example.moviedbappsbagus.MainActivity
import com.example.moviedbappsbagus.databinding.ItemMovieLayoutBinding
import com.example.moviedbappsbagus.model.Movies

class MovieAdapter (context: Context): RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    private var movieList = ArrayList<Movies>()
    fun setMovieList(movieList: List<Movies>) {
        this.movieList = movieList as ArrayList<Movies>
        notifyDataSetChanged()

    }
    val context = context
    class ViewHolder(val binding: ItemMovieLayoutBinding) : RecyclerView.ViewHolder(binding.root) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMovieLayoutBinding.inflate(
                LayoutInflater.from(
                    parent.context
                )
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       val  item = movieList[position]
        holder.binding.movieImage.setOnClickListener{
            val intent = Intent(context,DetailMovieActivity::class.java)
            intent.putExtra("idmovie" ,item.id.toString() )
            context.startActivity(intent)

        }
        Glide.with(holder.itemView)
            .load("https://image.tmdb.org/t/p/w500" + movieList[position].poster_path)
            .into(holder.binding.movieImage)
        holder.binding.movieName.text = item.title
        holder.binding.textViewDate.text ="tanggal rilis : "+ item.release_date
    }

    override fun getItemCount(): Int {
        return movieList.size
    }
}