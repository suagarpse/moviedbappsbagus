package com.example.moviedbappsbagus.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviedbappsbagus.DetailMovieActivity
import com.example.moviedbappsbagus.MainActivity
import com.example.moviedbappsbagus.R
import com.example.moviedbappsbagus.databinding.ItemLayoutReviewerBinding
import com.example.moviedbappsbagus.databinding.ItemMovieLayoutBinding
import com.example.moviedbappsbagus.model.Movies
import com.example.moviedbappsbagus.model.ReviewModel
import java.text.SimpleDateFormat

class ReviewerAdapter (context: Context): RecyclerView.Adapter<ReviewerAdapter.ViewHolder>() {
    private var movieList = ArrayList<ReviewModel>()
    fun setMovieList(movieList: List<ReviewModel>) {
        this.movieList = movieList as ArrayList<ReviewModel>
        notifyDataSetChanged()

    }
    val context = context
    class ViewHolder(val binding: ItemLayoutReviewerBinding) : RecyclerView.ViewHolder(binding.root) {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemLayoutReviewerBinding.inflate(
                LayoutInflater.from(
                    parent.context
                )
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item  = movieList[position]
//        holder.binding.imageReview.setOnClickListener{
//            val intent = Intent(context, DetailMoviesActivity::class.java)
//            intent.putExtra("idMovie", item.id.toString())
//            context.startActivity(intent)
//        }
        Glide.with(holder.itemView)
            .load("https://image.tmdb.org/t/p/w500" + item.authorDetails!!.avatarPath)
            .error(R.drawable.baseline_person_outline_24)
            .into(holder.binding.imageReview)
        holder.binding.textViewAuthor.text = item.author+" "
        holder.binding.textViewReview.text = item.content

        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val formatter = SimpleDateFormat("dd.MMMM.yyyy HH:mm")
        val output: String = formatter.format(parser.parse(item.createdAt.toString()))
        holder.binding.textViewCreated.text = output

    }

    override fun getItemCount(): Int {
        return movieList.size
    }
}