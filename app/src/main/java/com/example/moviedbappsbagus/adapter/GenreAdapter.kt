package com.example.moviedbappsbagus.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviedbappsbagus.DetailMovieActivity
import com.example.moviedbappsbagus.MainActivity
import com.example.moviedbappsbagus.R
import com.example.moviedbappsbagus.databinding.ItemListGenreBinding
import com.example.moviedbappsbagus.databinding.ItemMovieLayoutBinding
import com.example.moviedbappsbagus.model.GenresModel
import com.example.moviedbappsbagus.model.Movies
import java.util.Random

class GenreAdapter (private val mList: List<GenresModel>) : RecyclerView.Adapter<GenreAdapter.ViewHolder>() {

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_genre, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val ItemsViewModel = mList[position]

        holder.tvGenre.text = ItemsViewModel.name
        val rnd = Random()
        val color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        holder.tvGenre.setBackgroundColor(color)

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvGenre: TextView = itemView.findViewById(R.id.tvGenre)


    }
}