package com.example.moviedbappsbagus

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.moviedbappsbagus.databinding.ActivityDetailMovieBinding
import com.example.moviedbappsbagus.model.DetailMovies
import com.example.moviedbappsbagus.model.TrailerModel
import com.example.moviedbappsbagus.viewmodel.DetailMovieViewModel
import com.example.moviedbappsbagus.viewmodel.TrailerViewModel
import java.text.DecimalFormat

class DetailMovieActivity : AppCompatActivity() {
    private lateinit var binding : ActivityDetailMovieBinding
    private lateinit var viewModel: DetailMovieViewModel
    private lateinit var   lm : LinearLayoutManager
    var page = 1
    var trailer = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val idmovies = intent.getStringExtra("idmovie")
        binding = ActivityDetailMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getinit(idmovies!!)
        binding.buttonReview.setOnClickListener {
            val intent = Intent(this, ReviewerActivity::class.java)
            intent.putExtra("idmovie",idmovies.toString())
            startActivity(intent)
        }

    }
    fun getinit(idmovies:String)
    {


//        val ss:String = Intent().getStringExtra("idmovie").toString()
//        val idmovie = Intent().getStringExtra("idmovie").toString();
        // getting the string back
//        var bundle = Intent().extras
        val smovieids:String = Intent().getStringExtra("idmovie").toString()
        lm= LinearLayoutManager(this)
//        val movieid = bundle!!.getString("idmovie", "100");
        Log.d("okehbismillah", "onCreate: "+smovieids)
        viewModel = ViewModelProvider(this)[DetailMovieViewModel::class.java]
        viewModel.getMoviesDetail(idmovies.toString(),this)

        viewModel.observeMovieLiveData().observe(this, Observer { movieList ->

            binding.textViewtitle.text = movieList.title
            binding.textViewIsiReleaseDate.text = movieList.releaseDate
            binding.textViewIsiOverView.text = movieList.overview
            Glide.with(binding.imageMovie)
                .load("https://image.tmdb.org/t/p/w500" +movieList.posterPath)
                .into(binding.imageMovie)
            Glide.with(binding.imageBackdrop)
                .load("https://image.tmdb.org/t/p/w500" +movieList.backdropPath)
                .into(binding.imageBackdrop)
            val dec = DecimalFormat("#,###.##")
            val credits = dec.format(movieList.revenue)
            binding.textViewIsiRevenue.text = "\$ "+ credits
            binding.textViewIsiOverView.text = movieList.overview.toString()
            binding.textViewStatus.text = movieList.status.toString()

        })
        val viewModelTrailer = ViewModelProvider(this)[TrailerViewModel::class.java]
        viewModelTrailer.getTrailerDetail(idmovies.toString())

        viewModelTrailer.observeMovieLiveData().observe(this, Observer { movies->
            trailer= movies.key .toString()
        })
        binding.buttonViewTrailer.setOnClickListener {
            val uri = Uri.parse("https://www.youtube.com/watch?v=$trailer")
            startActivity(Intent(intent.action,uri))
        }

    }

}