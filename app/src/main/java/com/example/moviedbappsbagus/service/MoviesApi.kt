package com.example.moviedbappsbagus.service

import com.example.moviedbappsbagus.model.DetailMovies
import com.example.moviedbappsbagus.model.ResponseGenres
import com.example.moviedbappsbagus.model.ResponseListMovies
import com.example.moviedbappsbagus.model.ResponseReviewMovies
import com.example.moviedbappsbagus.model.ResponseVideosTrailer
import com.example.moviedbappsbagus.model.ResultListMovies
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap


interface MoviesApi {
    @GET("movie/now_playing")
    fun getNowPlaying( @QueryMap params : Map<String,String>) : Call<ResultListMovies>
    @GET("movie/popular?")
    fun getPopularMovies( @QueryMap params : Map<String,String>) : Call<ResultListMovies>
    @GET("movie/{movie_id}?")
    fun getDetailMovies(@Path("movie_id") idmovie:String, @QueryMap params : Map<String,String> ) : Call<DetailMovies>

    @GET("movie/{movie_id}/videos")
    fun getDetailVideosMovies(@Path("movie_id") idmovie:String, @QueryMap params : Map<String,String> ) : Call<ResponseVideosTrailer>
    @GET("search/movie?")
    fun getSearchMovie( @QueryMap params : Map<String,String>) : Call<ResultListMovies>
    @GET("discover/movie")
    fun getDiscovery( @QueryMap params : Map<String,String>) : Call<ResultListMovies>
    @GET("genre/movie/list")
    fun getGenre( @QueryMap params : Map<String,String>) : Call<ResponseGenres>
    @GET("movie/{movie_id}/reviews")
    fun getMoviesReviews(@Path("movie_id") idmovie:String, @QueryMap params : Map<String,String> ) : Call<ResponseReviewMovies>

}