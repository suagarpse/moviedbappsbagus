package com.example.moviedbappsbagus.fragment

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviedbappsbagus.adapter.GenreAdapter
import com.example.moviedbappsbagus.adapter.MovieAdapter
import com.example.moviedbappsbagus.databinding.FragmentListMoviesBinding
import com.example.moviedbappsbagus.model.GenresModel
import com.example.moviedbappsbagus.viewmodel.GenreViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [GenreFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GenreFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var binding: FragmentListMoviesBinding
    private lateinit var genreAdapter: GenreAdapter

    private lateinit var lm : LinearLayoutManager
    var     moviesnih :ArrayList<GenresModel> =ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }
    lateinit var viewModel:GenreViewModel
    private lateinit var movieAdapter : MovieAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentListMoviesBinding.inflate(layoutInflater)
        lm  = LinearLayoutManager(requireContext())

        prepareRecyclerViews()
        binding.textViewJudul.text = "Genre List"
        viewModel.observeMovieLiveData().observe(requireActivity(), Observer { movieList ->
            moviesnih.addAll(movieList)
            Log.d(TAG, "onCreateViewasaa: " + moviesnih)
            genreAdapter.notifyDataSetChanged()
        })

        return binding.root
    }

    private fun prepareRecyclerViews() {
        var lm = LinearLayoutManager(requireContext())
        binding.rvMovies.layoutManager =lm
        viewModel = ViewModelProvider(this)[GenreViewModel::class.java]
        viewModel.getGenre()
        genreAdapter = GenreAdapter(moviesnih)
        binding.rvMovies.setHasFixedSize(true)
        genreAdapter.notifyDataSetChanged()

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment GenreFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            GenreFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}