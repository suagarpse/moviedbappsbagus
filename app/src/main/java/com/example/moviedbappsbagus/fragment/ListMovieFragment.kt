package com.example.moviedbappsbagus.fragment

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviedbappsbagus.adapter.GenreAdapter
import com.example.moviedbappsbagus.adapter.MovieAdapter
import com.example.moviedbappsbagus.databinding.DialogCategoryLayoutBinding
import com.example.moviedbappsbagus.databinding.FragmentDashboardBinding
import com.example.moviedbappsbagus.databinding.FragmentListMoviesBinding
import com.example.moviedbappsbagus.model.GenresModel
import com.example.moviedbappsbagus.model.Movies
import com.example.moviedbappsbagus.viewmodel.GenreViewModel
import com.example.moviedbappsbagus.viewmodel.MovieViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog

class ListMovieFragment : Fragment() {

private var _binding: FragmentDashboardBinding? = null
  // This property is only valid between onCreateView and
  // onDestroyView.
  private var param1: String? = null
    private var param2: String? = null
    private lateinit var binding: FragmentListMoviesBinding
    private lateinit var viewModel: MovieViewModel
    private lateinit var viewModelGenre: GenreViewModel
    private lateinit var movieAdapter : MovieAdapter
    private lateinit var genreAdapter: GenreAdapter
    var     moviesnih :ArrayList<Movies> =ArrayList<Movies>()
    val listgenre = ArrayList<GenresModel>()

    private lateinit var lm : LinearLayoutManager
    private lateinit var lms : LinearLayoutManager
    var page = 1
    var search = false
    private lateinit var bindingCategory: DialogCategoryLayoutBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
  ): View {
        binding = FragmentListMoviesBinding.inflate(layoutInflater)
        lm  = LinearLayoutManager(requireContext())
        prepareRecyclerView()


        binding.imageViewSearch.setOnClickListener {
            binding.btnSearch.visibility = View.VISIBLE
            binding.editTextTextPersonName.visibility = View.VISIBLE
            binding.imageViewSearch.visibility = View.GONE
            binding.textViewJudul.visibility = View.GONE
        }
        viewModel = ViewModelProvider(this)[MovieViewModel::class.java]
        if(search==false)
        {
            viewModel.getPopularMovies(page)
            binding.rvMovies.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if(dy>0)
                    {
                        var vitem = lm.childCount
                        var litem = lm.findFirstCompletelyVisibleItemPosition()
                        var count = movieAdapter.itemCount
                        if(vitem+litem >count)
                        {
                            page+=1
                            loadmore()
                        }
                        Log.e("inivitem", "onScrolled: " + vitem.toString())

                    }

                }
            })
            viewModel.observeMovieLiveData().observe(requireActivity(), Observer { movieList ->
                moviesnih.addAll(movieList)
                movieAdapter.setMovieList(moviesnih)
            })
        }

        else if(search==true)
        {
            viewModel.observeMovieLiveData().observe(requireActivity(), Observer { movieList ->
                moviesnih.addAll(movieList)
                movieAdapter.setMovieList(moviesnih)
            })
        }
        binding.btnSearch.setOnClickListener {
            search=true
            viewModel.getsearch(binding.editTextTextPersonName.text.toString())
            clear()
            binding.btnSearch.visibility = View.GONE
            binding.editTextTextPersonName.visibility = View.GONE
            binding.imageViewSearch.visibility = View.VISIBLE
            binding.textViewJudul.visibility = View.VISIBLE
        }
        binding.imageViewCategory.setOnClickListener {
            showBottomSheetDialog()
        }

        return binding.root
    }
    private fun prepareRecyclerView() {

        viewModel = ViewModelProvider(this)[MovieViewModel::class.java]
        viewModel.getPopularMovies(page)
        movieAdapter = MovieAdapter(requireContext())

        binding.rvMovies.setHasFixedSize(true)
        binding.rvMovies.layoutManager = lm
        movieAdapter = MovieAdapter(requireContext())
        binding.rvMovies.adapter = movieAdapter

    }
    fun loadmore()
    {
        Handler().postDelayed(
            {

                viewModel.getPopularMovies(page)
                viewModel.observeMovieLiveData().observe(this, Observer { movieList ->
                    moviesnih.addAll(movieList)
//                    movieAdapter.setMovieList(moviesnih)
                    movieAdapter.notifyItemChanged(movieList.size)
                    Log.d(ContentValues.TAG, "loadmore:inipageke$page " + movieList[0])
                })
            },5000
        )
    }
    fun clear()
    {
        moviesnih.clear()

    }


    private fun showBottomSheetDialog() {
        bindingCategory = DialogCategoryLayoutBinding.inflate(layoutInflater)

        prepareRecyclerViewBottom()
        viewModelGenre = ViewModelProvider(this)[GenreViewModel::class.java]
        viewModelGenre.getGenre()

        viewModelGenre.observeMovieLiveData().observe(requireActivity(), Observer { movieList ->
            listgenre.addAll(movieList)
//                    movieAdapter.setMovieList(moviesnih)
            genreAdapter.notifyDataSetChanged()
            Log.d(TAG, "showBottomSheetDialog: bismillah" + movieList[0].toString())


        })
        val bottomSheetDialog = BottomSheetDialog(requireContext())
        bottomSheetDialog.setContentView(bindingCategory.root)

        bottomSheetDialog.show()
    }

    private fun prepareRecyclerViewBottom() {

        bindingCategory.mRecyclerView.setHasFixedSize(true)
        bindingCategory.mRecyclerView.layoutManager = lms
//        genreAdapter = GenreAdapter(requireContext(), movieList = )
        binding.rvMovies.adapter = movieAdapter

    }

}