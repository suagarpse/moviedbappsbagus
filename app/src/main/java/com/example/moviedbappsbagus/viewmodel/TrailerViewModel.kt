package com.example.moviedbappsbagus.viewmodel

import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviedbappsbagus.model.DetailMovies
import com.example.moviedbappsbagus.model.ResponseVideosTrailer
import com.example.moviedbappsbagus.model.TrailerModel
import com.example.moviedbappsbagus.service.RetrofitInstance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TrailerViewModel:ViewModel() {
    private var movieLiveData = MutableLiveData<TrailerModel>()
    fun getTrailerDetail(movieid:String) {
        val params: MutableMap<String, String> = HashMap()
        params["api_key"] = "7916ace8a965a1c3413cd5231af30364"
        params["language"] = "en-US"
        RetrofitInstance.api.getDetailVideosMovies(movieid,params).enqueue(object  :
            Callback<ResponseVideosTrailer> {
            override fun onResponse(call: Call<ResponseVideosTrailer>, response: Response<ResponseVideosTrailer>) {
                if (response.body()!=null){
                    val idvideos= response.body()!!.trailer[0].key

                    movieLiveData.value=response.body()!!.trailer[0]


                }
                else{
                    Log.d("TAGihan", response.code().toString())

                }
            }
            override fun onFailure(call: Call<ResponseVideosTrailer>, t: Throwable) {

                Log.d("TAGihan", t.message.toString())
            }
        })
    }
    fun observeMovieLiveData() : LiveData<TrailerModel> {
        return movieLiveData
    }
}