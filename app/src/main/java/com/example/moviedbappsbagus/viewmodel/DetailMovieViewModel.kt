package com.example.moviedbappsbagus.viewmodel

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviedbappsbagus.model.DetailMovies
import com.example.moviedbappsbagus.model.Movies
import com.example.moviedbappsbagus.service.RetrofitInstance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailMovieViewModel : ViewModel() {

    private var movieLiveData = MutableLiveData<DetailMovies>()
    fun getMoviesDetail(movieid:String,context:Context) {
        val params: MutableMap<String, String> = HashMap()
        params["api_key"] = "7916ace8a965a1c3413cd5231af30364"
        params["language"] = "en-US"
        RetrofitInstance.api.getDetailMovies(movieid,params).enqueue(object  :
            Callback<DetailMovies> {
            override fun onResponse(call: Call<DetailMovies>, response: Response<DetailMovies>) {
                if (response.body()!=null){
                    movieLiveData.value= response.body()!!
                }
                else{
                    Log.d("errorOnElse", response.code().toString())

                }
            }
            override fun onFailure(call: Call<DetailMovies>, t: Throwable) {
                Toast.makeText(context,"terjadi kesalahan",Toast.LENGTH_LONG)
                Log.d("errorOnfailure", t.message.toString())
            }
        })
    }
    fun observeMovieLiveData() : LiveData<DetailMovies> {
        return movieLiveData
    }
}