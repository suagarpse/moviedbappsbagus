package com.example.moviedbappsbagus.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviedbappsbagus.model.ResponseReviewMovies
import com.example.moviedbappsbagus.model.ReviewModel
import com.example.moviedbappsbagus.service.RetrofitInstance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ReviewMovieViewmodel : ViewModel() {
    private var movieLiveData = MutableLiveData<List<ReviewModel>>()
    fun getReviews(movieid :String, page:Int) {
        val params: MutableMap<String, String> = HashMap()
        params["api_key"] = "7916ace8a965a1c3413cd5231af30364"
        params["language"] = "en-US"
        params["page"] = page.toString()

        RetrofitInstance.api.getMoviesReviews(movieid, params).enqueue(object  :
            Callback<ResponseReviewMovies> {
            override fun onResponse(call: Call<ResponseReviewMovies>, response: Response<ResponseReviewMovies>) {
                if (response.body()!=null){
                    movieLiveData.value=response.body()!!.results
//                    Log.d(ContentValues.TAG, "inionResponse: "  + listMovies)
//                    reviewerAdapter.notifyDataSetChanged()
                }
                else{
                    Log.d("TAGihan", response.code().toString())

                }
            }
            override fun onFailure(call: Call<ResponseReviewMovies>, t: Throwable) {

                Log.d("TAGihan", t.message.toString())
            }
        })
    }
    fun observeMovieLiveData() : LiveData<List<ReviewModel>> {
        return movieLiveData
    }
}