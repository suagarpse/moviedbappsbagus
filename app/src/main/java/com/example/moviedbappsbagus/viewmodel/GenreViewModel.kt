package com.example.moviedbappsbagus.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviedbappsbagus.model.DetailMovies
import com.example.moviedbappsbagus.model.GenresModel
import com.example.moviedbappsbagus.model.Movies
import com.example.moviedbappsbagus.model.ResponseGenres
import com.example.moviedbappsbagus.service.RetrofitInstance
import com.google.android.material.bottomsheet.BottomSheetDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenreViewModel : ViewModel() {

    private var movieLiveData = MutableLiveData<List<GenresModel>>()

    fun getGenre() {
        val params: MutableMap<String, String> = HashMap()
        params["api_key"] = "7916ace8a965a1c3413cd5231af30364"
        params["language"] = "en-US"
        params["with_genres"] = ""
        RetrofitInstance.api.getGenre(params).enqueue(object  :
            Callback<ResponseGenres> {
            override fun onResponse(call: Call<ResponseGenres>, response: Response<ResponseGenres>) {
                if (response.body()!=null){
                    Log.d("TAGihansss", response.body()!!.genres[0].name)
                    movieLiveData.value=(response.body()!!.genres)
                }
                else{
                    Log.d("TAGihan", response.code().toString())

                }
            }
            override fun onFailure(call: Call<ResponseGenres>, t: Throwable) {

                Log.d("TAGihan", t.message.toString())
            }
        })
    }
    fun observeMovieLiveData() : MutableLiveData<List<GenresModel>> {
        return movieLiveData
    }




}