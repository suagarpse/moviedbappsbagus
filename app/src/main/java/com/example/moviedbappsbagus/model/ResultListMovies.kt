package com.example.moviedbappsbagus.model

data class ResultListMovies
    (
    val page: Int,
    val results: List<Movies>,
    val total_pages: Int,
    val total_results: Int
            )