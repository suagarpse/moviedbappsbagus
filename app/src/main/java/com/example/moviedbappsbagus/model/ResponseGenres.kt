package com.example.moviedbappsbagus.model

import com.google.gson.annotations.SerializedName

data class ResponseGenres
    (
    @SerializedName("genres" ) var genres : ArrayList<GenresModel> = arrayListOf()

)