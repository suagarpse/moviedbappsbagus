package com.example.moviedbappsbagus

import android.content.ContentValues
import android.content.ContentValues.TAG
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moviedbappsbagus.adapter.ReviewerAdapter
import com.example.moviedbappsbagus.databinding.ActivityReviewerBinding
import com.example.moviedbappsbagus.model.Movies
import com.example.moviedbappsbagus.model.ReviewModel
import com.example.moviedbappsbagus.viewmodel.ReviewMovieViewmodel

class ReviewerActivity : AppCompatActivity() {
    private lateinit var binding : ActivityReviewerBinding
    private lateinit var reviewerAdapter : ReviewerAdapter
    val listMovies = arrayListOf<ReviewModel>()
    var idmovie :String=""
    //    val  lm = LinearLayoutManager(this)
    var page = 1
    var     moviesnih :ArrayList<ReviewModel> =ArrayList<ReviewModel>()


    var search = false
    private lateinit var viewModel: ReviewMovieViewmodel

    var loading = true
    var pastItemsVisible: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    lateinit var lm  : LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReviewerBinding.inflate(layoutInflater)

        idmovie = intent.getStringExtra("idmovie").toString()

        prepareRecyclerView()

        getPopularMovies(idmovie,page)
        binding.mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if(dy>0)
                {
                    var vitem = lm.childCount
                    var litem = lm.findFirstCompletelyVisibleItemPosition()
                    var count = reviewerAdapter.itemCount
                    if(vitem+litem >count)
                    {
                        page+=1
                        loadmore()
                    }
                    Log.e("inivitem", "onScrolled: " + vitem.toString())

                }

            }
        })
        viewModel.observeMovieLiveData().observe(this, Observer { movieList ->
            moviesnih.addAll(movieList)
            reviewerAdapter.setMovieList(moviesnih)
        })
        setContentView(binding.root)
    }

    private fun getPopularMovies(idmovie: String, page: Int) {
        viewModel = ViewModelProvider(this)[ReviewMovieViewmodel::class.java]
        viewModel.getReviews(idmovie,page)
        viewModel.observeMovieLiveData().observe(this, Observer { movieList ->
            moviesnih.addAll(movieList)
            Log.d(TAG, "getPopularMovies: isireviewernya" + movieList)
            ReviewerAdapter(this).setMovieList(moviesnih)
        })
    }

    private fun prepareRecyclerView() {
        lm = LinearLayoutManager(this)

        reviewerAdapter = ReviewerAdapter(this)

        binding.mRecyclerView.setHasFixedSize(true)
        binding.mRecyclerView.layoutManager = lm
        reviewerAdapter = ReviewerAdapter(this)
        binding.mRecyclerView.adapter = reviewerAdapter


        binding.mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) { //check for scroll down
                    visibleItemCount = lm.getChildCount()
                    totalItemCount = lm.getItemCount()
                    pastItemsVisible = lm.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastItemsVisible >= totalItemCount) {
                            loading = false
                            Log.v("...", "Last Item !")
                            loadmore()
                            // Do pagination.. i.e. fetch new data
                            loading = true
                        }
                    }
                }
            }
        })


    }
    fun loadmore()
    {
        Handler().postDelayed(
            {

                viewModel.getReviews(idmovie,page)
                viewModel.observeMovieLiveData().observe(this, Observer { movieList ->
                    moviesnih.addAll(movieList)
//                    movieAdapter.setMovieList(moviesnih)
                    reviewerAdapter.notifyItemChanged(movieList.size)
//                    Log.d(ContentValues.TAG, "loadmore:inipageke$page " + movieList[0])
                })
            },5000
        )
    }
    fun clear()
    {
        moviesnih.clear()

    }
}